use mdbook::book::Book;
use mdbook::preprocess::{Preprocessor, PreprocessorContext};
use std::collections::hash_map::DefaultHasher;
use std::ffi::OsStr;
use std::fmt;
use std::fs;
use std::fs::File;
use std::hash::Hasher;
use std::io::prelude::*;
use std::path::PathBuf;
use std::str;
use std::{path::Path, process::Command};
use thiserror::Error;

#[derive(Error, Debug)]
struct CmdError {
    msg: String,
    output: std::process::Output,
}

impl fmt::Display for CmdError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}: {:?}", self.msg, self.output)
    }
}

enum Error {
    IoError(std::io::Error),
    CmdError(CmdError),
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::IoError(e)
    }
}

impl From<Error> for mdbook::errors::Error {
    fn from(e: Error) -> mdbook::errors::Error {
        match e {
            Error::IoError(e) => {
                // mdbook::errors::Error::from_kind(mdbook::errors::ErrorKind::Io(e))
                mdbook::errors::Error::new(e)
            }
            Error::CmdError(e) => {
                // mdbook::errors::Error::from_kind(mdbook::errors::ErrorKind::Subprocess(e.msg, e.output),
                mdbook::errors::Error::new(e)
            }
        }
    }
}

/// Write the string to a file at the path.
fn write_file(path: &Path, s: String) -> Result<(), std::io::Error> {
    let mut file = File::create(path)?;
    file.write_all(s.as_bytes())
}

/// Return a string of the files hash value
fn hash_file<T: Into<PathBuf>>(path: T) -> Result<String, std::io::Error> {
    let mut hasher = DefaultHasher::new();
    let s = std::fs::read_to_string(path.into())?;
    hasher.write(s.as_bytes());

    Ok(hasher.finish().to_string())
}

/// Read the string value of the hash from the hash file
fn read_hash(path: &Path) -> Result<Option<String>, std::io::Error> {
    if let Some(path) = hash_path(&path) {
        if path.is_file() {
            let s = std::fs::read_to_string::<PathBuf>(path)?;
            return Ok(Some(s));
        }
    }
    Ok(None)
}

/// Derive the path of the hash file for a diagram
fn hash_path<T: Into<PathBuf>>(path: T) -> Option<PathBuf> {
    let mut p = path.into();
    if let Some(file_name) = p.file_name() {
        if let Some(s) = file_name.to_str() {
            let mut new_file_name = String::from(".");
            new_file_name.push_str(s);
            p.set_file_name(new_file_name);
            return Some(p);
        }
    }
    None
}

fn write_png<T: Into<String>>(s: T) -> Result<(), Error> {
    let mut cmd = String::from("plantuml ");
    cmd.push_str(&s.into());

    let output = Command::new("sh").args(&["-c", &cmd]).output()?;

    if !output.status.success() {
        Err(Error::CmdError(CmdError {
            msg: String::from("PlantUML failed to render."),
            output,
        }))
    } else {
        Ok(())
    }
}

fn is_uml<T: Into<PathBuf>>(path: T) -> bool {
    let path = path.into();
    if let Some(p) = path.file_name() {
        if !p.to_str().map(|s| s.starts_with('.')).unwrap_or(false) {
            return path.extension() == Some(OsStr::new("puml"));
        }
    }

    false
}

fn is_cached<T: Into<PathBuf>>(path: T) -> Result<bool, std::io::Error> {
    let p = path.into();
    let h = hash_file(&p)?;
    if let Some(h_read) = read_hash(&p)? {
        if h == h_read {
            // hashes match, looks like the diagram is cached
            return Ok(true);
        }
    }
    Ok(false)
}

fn process_directory<P: Into<PathBuf>>(path: P) -> Result<(), Error> {
    // If the path does not exist then create it
    let path = path.into();
    if !path.is_dir() {
        fs::create_dir(&path)?;
    }

    // Find files
    for p in (fs::read_dir(path)?).flatten() {
        if is_uml(p.path()) {
            // is the diagram cached?
            if !is_cached(p.path())? {
                // write the hash value to cache the diagram
                if let Some(h_path) = hash_path(&p.path()) {
                    let h = hash_file(p.path())?;
                    write_file(&h_path, h)?;
                }
                // render and write the diagram
                if let Ok(s) = p.path().into_os_string().into_string() {
                    write_png(s)?;
                }
            }
        }
    }

    Ok(())
}

pub struct PlantUML;

impl Default for PlantUML {
    fn default() -> PlantUML {
        PlantUML
    }
}

impl PlantUML {
    pub fn new() -> PlantUML {
        PlantUML
    }
}

impl Preprocessor for PlantUML {
    fn name(&self) -> &str {
        "plantuml-preprocessor"
    }

    fn run(&self, _ctx: &PreprocessorContext, book: Book) -> Result<Book, mdbook::errors::Error> {
        process_directory::<PathBuf>("src/uml".into())?;
        Ok(book)
    }

    fn supports_renderer(&self, renderer: &str) -> bool {
        renderer != "not-supported"
    }
}
