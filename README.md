# MDBook PlantUML preprocessor

## Getting Started

Before you can use the preprocessor you'll need to install it. Check out the repo and build for install.
```sh
$ cargo install --path . --bin mdbook-plantuml
```

Next you need to let `mdbook` know to use the preprocessor by updating
your `book.toml` file.

```toml
[book]
authors = ["me"]
multilingual = false
src = "src"
title = "my awesome book"

[preprocessor.plantuml]
```

Now you're set to add PlantUML diagrams to your book.
1. Place your `<file_name>.puml` file in `src/uml/`.
2. Reference the `<file_name>.png` file that will be generated.
```md
![alt text](./uml/<file_name>.png "PlantUML diagram")
```
3. Run MDBook as you normally would 👍